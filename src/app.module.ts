import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CodeGeneratorService } from './services/codegenerator.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [CodeGeneratorService],
})
export class AppModule {}
