import { HttpException, HttpStatus } from "@nestjs/common";

export class ForbiddenException extends HttpException {
  constructor() {
    super('Forbidden', HttpStatus.FORBIDDEN);
  }
}

export class NotFoundException extends HttpException {
  constructor() {
    super('Not Found', HttpStatus.NOT_FOUND);
  }
}

export class CodeSpecificationException extends HttpException {
  constructor() {
    super('code: must be provided', HttpStatus.BAD_REQUEST);
  }
}

export class NotImplementedMethodException extends HttpException {
  constructor() {
    super('combination with repetitions are not implemented yet', HttpStatus.NOT_IMPLEMENTED);
  }
}

export class ValidationException extends HttpException {
  constructor(message: string) {
    super(message, HttpStatus.EXPECTATION_FAILED);
  }
}