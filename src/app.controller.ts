import { Body, Controller, Post } from '@nestjs/common';
import { CodeSpecificationException } from './common/exceptions/custom.exceptions';
import { CustomRequest, CustomResponse } from './services/code.model';
import { CodeGeneratorService } from './services/codegenerator.service';
import * as _ from "lodash";

@Controller()
export class AppController {

  constructor(private readonly service: CodeGeneratorService) { }

  @Post()
  async getCodes(@Body() body: CustomRequest): Promise<CustomResponse> {
    return this.service.getCodes(body.code);
  }
}
