import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppModule } from './app.module';
import { Code, CustomRequest, CustomResponse } from './services/code.model';
import { CodeGeneratorService } from './services/codegenerator.service';
import express from 'express';
import bodyParser from 'body-parser';

describe('AppController', () => {
  let app: INestApplication;
  let controller: AppController;
  let service: CodeGeneratorService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(CodeGeneratorService)
      .useValue(service)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();

    controller = app.get<AppController>(AppController);
    service = app.get<CodeGeneratorService>(CodeGeneratorService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should run combination to calculate', async () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: false, repetitions: false } }
    const result: CustomResponse = await controller.getCodes(body)
    expect(result.method).toBe('combination');
  });

  it('should run combination with repetitions', async () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: false, repetitions: true } }
    const result: CustomResponse = await controller.getCodes(body)
    expect(result.method).toBe('combination with repetitions');
  });

  it('should run permutation to calculate', async () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: true, repetitions: false } }
    const result: CustomResponse = await controller.getCodes(body)
    expect(result.method).toBe('permutation');
  });

  it('should run permutation with repetitions to calculate', async () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: true, repetitions: true } }
    const result: CustomResponse = await controller.getCodes(body)
    expect(result.method).toBe('permutation with repetitions');
  });

  it(`/POST combinations`, () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: false, repetitions: false } }
    return request(app.getHttpServer())
      .post('/')
      .send(body)
      .set('Accept', '/application\/json/')
      .expect(service.getCodes(body.code));
  });

  it(`/POST combinations with repetitions`, () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: false, repetitions: true } }
    return request(app.getHttpServer())
      .post('/')
      .send(body)
      .set('Accept', '/application\/json/')
      .expect(service.getCodes(body.code));
  });

  it(`/POST permutations`, () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: true, repetitions: false } }
    return request(app.getHttpServer())
      .post('/')
      .send(body)
      .set('Accept', '/application\/json/')
      .expect(service.getCodes(body.code));
  });

  it(`/POST permutations with repetitions`, () => {
    const body: CustomRequest = { code: { numeralsystem: 'numeric', size: 4, ordermatter: true, repetitions: true } }
    return request(app.getHttpServer())
      .post('/')
      .send(body)
      .set('Accept', '/application\/json/')
      .expect(service.getCodes(body.code));
  });

  afterAll(async () => {
    await app.close();
  });

});
