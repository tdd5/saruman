import { CBase } from "./CBase";

export class Combination extends CBase {

  constructor(seed: Iterable<string>, size: number) {
    super();
    this.method = 'combination';
    this.seed = [...seed];
    this.size = size;
    this.length = this.calculate(this.seed.length, this.size);
  }

  protected calculate(n: number, r: number): number {
    if (n == r) return 1;
    return Math.trunc(this.factorial(n) / (this.factorial(r) * this.factorial(n - r)));
  };

  protected solution(): Array<any> {
    const result = [];
    if (this.size > this.seed.length) return result;
    combination(this.seed, [], 0, this.seed.length, this.size, result);
    return result;
  }
}

const combination = (arr: Array<any>, chosen: Array<any>, index: number, n: number, r: number, result: Array<any>) => {
  for (let i = index; i < n; i++) {
    const next = [...chosen, arr[i]];
    if (r == 1) {
      result.push(next);
    }
    else {
      combination(arr, next, i + 1, n, r - 1, result);
    }
  }
}