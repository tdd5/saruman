import { CBase } from "./CBase";

export class PermutationRepetitions extends CBase {

  constructor(seed: Iterable<string>, size: number) {
    super();
    this.method = 'permutation with repetitions';
    this.seed = [...seed];
    this.size = size;
    this.length = this.calculate(this.seed.length, this.size);
  }

  protected calculate(n: number, k: number): number {
    return Math.pow(n, k);
  }

  protected solution(): Array<any> {
    let result = [];
    permute(this.seed, [], this.size, result);
    return result;
  };
}

const permute = (arr: Array<any>, chosen: Array<any> = [], r: number, result: Array<any>) => {
  if (chosen.length === r) {
    result.push(chosen);
  } else {
    for (let i of arr) {
      permute(arr, chosen.concat(i), r, result);
    }
  }
};