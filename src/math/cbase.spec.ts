import { Combination, PermutationRepetitions, Permutation, CombinationRepetitions } from '.';

describe('permutation with repetitions', () => {
  it('should generate 27 permutation with repetitions', () => {
    const seed = '012';
    const size = 3;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    expect(permutationRepetitions.length).toBe(27);
  });

  it('should generate 1.000 permutation with repetitions', () => {
    const seed = '0123456789';
    const size = 3;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    expect(permutationRepetitions.length).toBe(1000);
  });

  it('should generate 10.000 permutation with repetitions', () => {
    const seed = '0123456789';
    const size = 4;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    expect(permutationRepetitions.length).toBe(10000);
  });

  it('should generate 1.679.616 permutation with repetitions', () => {
    const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
    const size = 4;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    expect(permutationRepetitions.length).toBe(1679616);
  });

  it('should generate 2.176.782.336 permutation with repetitions', () => {
    const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
    const size = 6;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    expect(permutationRepetitions.length).toBe(2176782336);
  });

  it('should be unique all PermutationRepetitions for 0-9 digits in groups of 4', () => {
    const seed = '0123456789';
    const size = 4;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    const result = permutationRepetitions.toArray();
    const duplicates = result.filter((item, index) => index !== result.indexOf(item));
    expect(duplicates.length).toBe(0);
  });

  it('should be equals PermutationRepetitions.lenght and array.length for 10 elements', () => {
    const seed = '0123456789';
    const size = 4;
    const permutationRepetitions = new PermutationRepetitions(seed, size);
    const result = permutationRepetitions.toArray();
    expect(permutationRepetitions.length).toBe(result.length);
  });

});

describe('Combination', () => {

  it('should generate 3 combinations', () => {
    const seed = '012';
    const size = 2;
    const combination = new Combination(seed, size);
    expect(combination.length).toBe(3);
  });

  it('should generate 120 combinations', () => {
    const seed = '0123456789';
    const size = 3;
    const combination = new Combination(seed, size);
    expect(combination.length).toBe(120);
  });

  it('should generate 210 combinations', () => {
    const seed = '0123456789';
    const size = 4;
    const combination = new Combination(seed, size);
    expect(combination.length).toBe(210);
  });

  it('should generate 58.905 combinations', () => {
    const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
    const size = 4;
    const combination = new Combination(seed, size);
    expect(combination.length).toBe(58905);
  });

  it('should generate 1.947.792 combinations', () => {
    const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
    const size = 6;
    const combination = new Combination(seed, size);
    expect(combination.length).toBe(1947792);
  });

  it('should be unique all combinations for 0-9 digits in groups of 4', () => {
    const seed = '0123456789';
    const size = 4;
    const combination = new Combination(seed, size);
    const result = combination.toArray();
    const duplicates = result.filter((item, index) => index !== result.indexOf(item));
    expect(duplicates.length).toBe(0);
  });

  it('should be equals combination.lenght and array.length for 10 elements', () => {
    const seed = '0123456789';
    const size = 4;
    const combination = new Combination(seed, size);
    const result = combination.toArray();
    expect(combination.length).toBe(result.length);
  });

});


describe('Permutation', () => {

  it('should generate 6 Permutation', () => {
    const seed = '012';
    const size = 3;
    const permutation = new Permutation(seed, size);
    expect(permutation.length).toBe(6);
  });

  it('should generate 720 Permutation', () => {
    const seed = '0123456789';
    const size = 3;
    const permutation = new Permutation(seed, size);
    expect(permutation.length).toBe(720);
  });

  it('should generate 5.040 Permutation', () => {
    const seed = '0123456789';
    const size = 4;
    const permutation = new Permutation(seed, size);
    expect(permutation.length).toBe(5040);
  });

  it('should generate 1.413.720 Permutation', () => {
    const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
    const size = 4;
    const permutation = new Permutation(seed, size);
    expect(permutation.length).toBe(1413720);
  });

  it('should generate 1.402.410.240 permutations', () => {
    const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
    const size = 6;
    const permutation = new Permutation(seed, size);
    expect(permutation.length).toBe(1402410240);
  });

  it('should be unique all Permutation for 0-9 digits in groups of 4', () => {
    const seed = '0123456789';
    const size = 4;
    const permutation = new Permutation(seed, size);
    const result = permutation.toArray();
    const duplicates = result.filter((item, index) => index !== result.indexOf(item));
    expect(duplicates.length).toBe(0);
  });

  it('should be equals permutation.lenght and array.length for 10 elements', () => {
    const seed = '0123456789';
    const size = 4;
    const permutation = new Permutation(seed, size);
    const result = permutation.toArray();
    expect(permutation.length).toBe(result.length);
  });

  describe('Combination with repetitions', () => {

    it('should generate 20 combinations with repetitions', () => {
      const seed = '0123';
      const size = 3;
      const permutation = new CombinationRepetitions(seed, size);
      expect(permutation.length).toBe(20);
    });

    it('should generate 715 combinations with repetitions', () => {
      const seed = '0123456789';
      const size = 4;
      const combination = new CombinationRepetitions(seed, size);
      expect(combination.length).toBe(715);
    });

    it('should generate 82251 combinations with repetitions', () => {
      const seed = '0123456789abcdefghijklmnopqrstuvwxyz';
      const size = 4;
      const combination = new CombinationRepetitions(seed, size);
      expect(combination.length).toBe(82251);
    });

    it('should be unique all combinations with repetitions for 0-9 digits in groups of 4', () => {
      const seed = '0123456789';
      const size = 4;
      const combination = new CombinationRepetitions(seed, size);
      const result = combination.toArray();
      const duplicates = result.filter((item, index) => index !== result.indexOf(item));
      expect(duplicates.length).toBe(0);
    });

    it('should be equals combinations.lenght and array.length for 10 elements', () => {
      const seed = '0123456789';
      const size = 4;
      const combination = new CombinationRepetitions(seed, size);
      const result = combination.toArray();
      expect(combination.length).toBe(result.length);
    });

  });

});