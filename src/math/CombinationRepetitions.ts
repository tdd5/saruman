import { CBase } from "./CBase";

export class CombinationRepetitions extends CBase {

  constructor(seed: Iterable<string>, size: number) {
    super();
    this.method = 'combination with repetitions';
    this.seed = [...seed];
    this.size = size;
    this.length = this.calculate(this.seed.length, this.size);
  }

  protected calculate(n: number, r: number): number {
    return Math.trunc(this.factorial(n + r - 1) / (this.factorial(n - 1) * this.factorial(r)));
  };

  protected solution(): Array<any> {
    const result = [];
    var chosen = Array.from({ length: (this.size + 1) }, (_, i) => 0);
    CombinationRepetition(this.seed, chosen, 0, this.size, 0, this.seed.length - 1, result);
    return result;
  }
}

const CombinationRepetition = (arr: Array<any>, chosen: Array<any>, index: number, r: number, start: number, end: number, result: Array<any>) => {
  if (index == r) {
    let subset = [];
    for (var i = 0; i < r; i++) {
      subset.push(arr[chosen[i]]);
    }
    result.push(subset);
    return;
  }
  for (var i = start; i <= end; i++) {
    chosen[index] = i;
    CombinationRepetition(arr, chosen, index + 1, r, i, end, result);
  }
  return;
}