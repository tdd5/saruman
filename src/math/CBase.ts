export class CBase {
  [Symbol.iterator]() {
    return function* (it) {
      for (let i of it.solution()) yield i;
    }(this);
  }
  toArray() {
    return [...this];
  }

  protected seed: string[];
  protected size: number;
  length: number;
  method: string;
  protected solution(): Array<any> { return [] };

  protected factorial(num: number): number {
    if (num < 0)
      return -1;
    else if (num == 0)
      return 1;
    else {
      return (num * this.factorial(num - 1));
    }
  };
}