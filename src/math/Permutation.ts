import { CBase } from "./CBase";

export class Permutation extends CBase {

  constructor(seed: Iterable<string>, size: number) {
    super();
    this.method = 'permutation';
    this.seed = [...seed];
    this.size = size;
    this.length = this.calculate(this.seed.length, this.size);
  }

  protected calculate(n: number, k: number): number {
    if (0 == k) return 1;
    if (n < k) return 0;
    var p = this.factorial(n);
    var v = this.factorial(n - k);
    return Math.trunc(p / v);
  }

  protected solution(): Array<any> {
    let results = [];
    results = permute(this.seed, this.seed.length, this.size);
    return results;
  };
}

const permute = (arr: Array<any>, n: number, r: number): Array<any>  => {
  if (r > n) return [];
  else if (r == 1) return arr.map(d => [d]);
  return arr.flatMap(d => permute(arr.filter(a => a !== d), n, r - 1).map((item: any) => [d, ...item]));
};