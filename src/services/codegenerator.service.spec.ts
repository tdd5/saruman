import { Test, TestingModule } from '@nestjs/testing';
import { Code } from './code.model';
import { CodeGeneratorService } from './codegenerator.service';

describe('Code Generator Service', () => {
  let service: CodeGeneratorService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [CodeGeneratorService],
    }).compile();

    service = app.get<CodeGeneratorService>(CodeGeneratorService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should generate 210 combinations', () => {
    const code: Code = { numeralsystem: 'numeric', size: 4, ordermatter: false, repetitions: false };
    const result = service.getCodes(code);
    expect(result.length).toBe(210);
  });

  it('should generate 715 combinations with repetitions', () => {
    const code: Code = { numeralsystem: 'numeric', size: 4, ordermatter: false, repetitions: true };
    const result = service.getCodes(code);
    expect(result.length).toBe(715);
  });

  it('should generate 5.040 Permutation', () => {
    const code: Code = { numeralsystem: 'numeric', size: 4, ordermatter: true, repetitions: false };
    const result = service.getCodes(code);
    expect(result.length).toBe(5040);
  });

  it('should generate 10.000 permutation with repetitions', () => {
    const code: Code = { numeralsystem: 'numeric', size: 4, ordermatter: true, repetitions: true };
    const result = service.getCodes(code);
    expect(result.length).toBe(10000);
  });

});
