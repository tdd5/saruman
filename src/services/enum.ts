export enum NumeralSystem {
  numeric = '0123456789',
  alphabetic = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  alphanumeric = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
}

export enum math {
  Permutation,
  Combination,
  PermutationRepetitions,
  CombinationRepetitions
}