import { Dictionary } from "lodash";
import { Combination, CombinationRepetitions, Permutation, PermutationRepetitions } from "./../math";
import { CBase } from "./../math/CBase";
import { Code } from "./code.model";
import { math, NumeralSystem } from "./enum";

export class CodeManagerContext {
  private strategy: Dictionary<CBase>;
  private code: Code;

  constructor(code: Code) {
    this.strategy = {};
    this.code = code;
    this.createStrategy();
  }

  private createStrategy() {
    const seed: string = NumeralSystem[this.code.numeralsystem];
    this.strategy[math.Permutation] = new Permutation(seed, this.code.size);
    this.strategy[math.PermutationRepetitions] = new PermutationRepetitions(seed, this.code.size);
    this.strategy[math.Combination] = new Combination(seed, this.code.size);
    this.strategy[math.CombinationRepetitions] = new CombinationRepetitions(seed, this.code.size);
  }

  public getCodeManagerStrategy(): CBase {
    const method: number = checkCombination(this.code.ordermatter, this.code.repetitions);
    return this.strategy[method];
  }

}

const checkCombination = (ordermatter: boolean, repetitions: boolean) => {
  return (!ordermatter && !repetitions) ? math.Combination : checkCombinationWithRepetitions(ordermatter, repetitions);
}

const checkCombinationWithRepetitions = (ordermatter: boolean, repetitions: boolean) => {
  return (!ordermatter && repetitions) ? math.CombinationRepetitions : checkPermutation(ordermatter, repetitions);
}

const checkPermutation = (ordermatter: boolean, repetitions: boolean) => {
  return (ordermatter && !repetitions) ? math.Permutation : checkPermutationWithRepetitions(ordermatter, repetitions);
}

const checkPermutationWithRepetitions = (ordermatter: boolean, repetitions: boolean) => {
  return (ordermatter && repetitions) ? math.PermutationRepetitions : -1;
}