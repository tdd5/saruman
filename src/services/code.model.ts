import { Type } from "class-transformer";
import { IsNotEmpty, IsNumber, ValidateNested, Min, IsString, Max, IsBoolean, IsOptional, IsIn } from "class-validator";

export class Code {
  @IsNotEmpty({  message: 'numeral system: must be provided' })
  @IsString()
  @IsIn(['numeric', 'alphabetic', 'alphanumeric'])
  numeralsystem: string;

  @IsOptional()
  @IsBoolean()
  ordermatter: boolean = false;

  @IsOptional()
  @IsBoolean()
  repetitions: boolean = false;
  
  @IsNotEmpty({  message: 'size: must be provided' })
  @IsNumber()
  @Min(2)
  @Max(6)
  size: number;
}

export class CustomRequest {
  @IsNotEmpty({  message: 'code: object must be provided in the body' })
  @ValidateNested({ each: true })
  @Type(() => Code)
  code: Code;
}

export class CustomResponse {
  groups: Array<string>;
  length: number;
  method: string;
}

export const getCustomResponse = (groups: Array<string>, length: number, method: string): CustomResponse => {
  return {
    groups: groups,
    length: length,
    method: method
  };
};