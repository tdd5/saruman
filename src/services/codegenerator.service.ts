import { Injectable } from '@nestjs/common';
import { Code, CustomResponse, getCustomResponse } from './code.model';
import { CodeManagerContext } from './codeManager.context';

@Injectable()
export class CodeGeneratorService {
  getCodes (code: Code): CustomResponse {
    const codeManagerContext: CodeManagerContext = new CodeManagerContext(code);
    const codes = codeManagerContext.getCodeManagerStrategy();
    return getCustomResponse(codes.toArray(), codes.length, codes.method);
  }
}